---
- name: Install iptables
  package: name={{ item }} state=present
  with_items:
    - iptables

- name: Install iptables-persistent [RedHat/CentOS]
  package: name={{ item }} state=present
  with_items:
    - iptables-services
    - iptables-utils

- name: Allow related and established connections ipv4
  iptables:
    action: insert
    chain: INPUT
    ctstate: ESTABLISHED,RELATED
    jump: ACCEPT
    ip_version: ipv4
    rule_num: 1

- name: Allow related and established connections ipv6
  iptables:
    action: insert
    chain: INPUT
    ctstate: ESTABLISHED,RELATED
    jump: ACCEPT
    ip_version: ipv6
    rule_num: 1

- name: Allow all local traffic inbound on the `lo` interface ipv4
  iptables:
    action: insert
    chain: INPUT
    in_interface: lo
    jump: ACCEPT
    ip_version: ipv4
    rule_num: 2

- name: Allow all local traffic outbound on the `lo` interface ipv4
  iptables:
    chain: OUTPUT
    out_interface: lo
    jump: ACCEPT
    ip_version: ipv4

- name: Allow all local traffic inbound on the `lo` interface ipv6
  iptables:
    action: insert
    chain: INPUT
    in_interface: lo
    jump: ACCEPT
    ip_version: ipv6
    rule_num: 2

- name: Allow all local traffic outbound on the `lo` interface ipv6
  iptables:
    chain: OUTPUT
    out_interface: lo
    jump: ACCEPT
    ip_version: ipv6

- name: Prevent syn packet flood ipv4
  iptables:
    action: insert
    chain: INPUT
    protocol: tcp
    syn: negate
    ctstate: NEW
    jump: DROP
    ip_version: ipv4
    rule_num: 3

- name: Prevent syn packet flood ipv6
  iptables:
    action: insert
    chain: INPUT
    protocol: tcp
    syn: negate
    ctstate: NEW
    jump: DROP
    ip_version: ipv6
    rule_num: 3

- name: Allow ping traffic ipv4
  iptables:
    action: insert
    chain: INPUT
    protocol: icmp
    icmp_type: echo-request
    jump: ACCEPT
    ip_version: ipv4
    rule_num: 4

- name: Allow ping traffic inbound ipv6
  iptables:
    action: insert
    chain: INPUT
    protocol: icmpv6
    jump: ACCEPT
    ip_version: ipv6
    rule_num: 4

- name: Allow ping traffic outbound ipv6
  iptables:
    chain: OUTPUT
    protocol: icmpv6
    jump: ACCEPT
    ip_version: ipv6

- name: Allow DHCPv6 from LAN only
  iptables:
    chain: INPUT
    protocol: udp
    destination_port: 546
    source: fe80::/10
    ctstate: NEW,ESTABLISHED
    jump: ACCEPT
    ip_version: ipv6

- name: Allow ssh ipv4
  iptables:
    action: insert
    chain: INPUT
    protocol: tcp
    destination_port: 22
    ctstate: NEW,ESTABLISHED
    jump: ACCEPT
    ip_version: ipv4
    rule_num: 5

- name: Allow ssh ipv6
  iptables:
    action: insert
    chain: INPUT
    protocol: tcp
    destination_port: 22
    ctstate: NEW,ESTABLISHED
    jump: ACCEPT
    ip_version: ipv6
    rule_num: 5

- name: Drop all inbound packets not matching those explicitly allowed ipv4
  iptables:
    chain: INPUT
    policy: DROP
    ip_version: ipv4

- name: Drop all inbound packets not matching those explicitly allowed ipv6
  iptables:
    chain: INPUT
    policy: DROP
    ip_version: ipv6

- name: Disable firewalld service
  service:
    name: firewalld
    enabled: no
  when: ansible_os_family == "RedHat"

- name: Enable iptables service
  service:
    name: iptables
    enabled: yes
  when: ansible_os_family == "RedHat"

- name: Save v4 rules
  raw: service iptables save

- name: Save v6 rules
  raw: service ip6tables save
